import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let fighterOne = {
      hpContainer: document.getElementById('left-fighter-indicator'),
      fighter: firstFighter,
      health: firstFighter.health,
      blockCriticalAttack: false,
    };
    let fighterTwo = {
      hpContainer: document.getElementById('right-fighter-indicator'),
      fighter: secondFighter,
      health: secondFighter.health,
      blockCriticalAttack: false,
    };
    let pressedKeys = new Set();

    document.addEventListener('keydown', (event) => {
      pressedKeys.add(event.code);
      if (
        !fighterOne.blockCriticalAttack &&
        controls.PlayerOneCriticalHitCombination.includes(event.code) &&
        checkKeysCriticalAttack(controls.PlayerOneCriticalHitCombination, pressedKeys)
      ) {
        attack(fighterOne, fighterTwo, true);
      }
      if (
        !fighterTwo.blockCriticalAttack &&
        controls.PlayerTwoCriticalHitCombination.includes(event.code) &&
        checkKeysCriticalAttack(controls.PlayerTwoCriticalHitCombination, pressedKeys)
      ) {
        attack(fighterTwo, fighterOne, true);
      }
      switch (event.code) {
        case controls.PlayerOneAttack:
          if (!pressedKeys.has(controls.PlayerOneBlock) && !pressedKeys.has(controls.PlayerTwoBlock)) {
            attack(fighterOne, fighterTwo);
          }
          break;
        case controls.PlayerTwoAttack:
          if (!pressedKeys.has(controls.PlayerTwoBlock) && !pressedKeys.has(controls.PlayerOneBlock)) {
            attack(fighterTwo, fighterOne);
          }
          break;
      }
    });

    document.addEventListener('keyup', (event) => {
      pressedKeys.delete(event.code);
    });

    function attack(attacker, defender, isCritical = false) {
      let damage;
      if (isCritical) {
        damage = getCriticalDamage(attacker.fighter);
        attacker.blockCriticalAttack = true;
        setTimeout(() => {
          attacker.blockCriticalAttack = false;
        }, 10000);
      } else {
        damage = getDamage(attacker.fighter, defender.fighter);
      }
      defender.health -= damage;
      setHpInBar(defender);
      if (defender.health <= 0) resolve(attacker.fighter);
    }
  });
}

export function getDamage(firstFighter, secondFighter) {
  const hitPower = getHitPower(firstFighter);
  const blockPower = getBlockPower(secondFighter);
  let damage = hitPower - blockPower;
  damage = damage > 0 ? damage : 0;
  return damage;
}

export function getHitPower(fighter) {
  const power = fighter.attack * (Math.random() + 1);
  return power;
}

export function getBlockPower(fighter) {
  const power = fighter.defense * (Math.random() + 1);
  return power;
}

export function getCriticalDamage(fighter) {
  const damage = fighter.attack * 2;
  return damage;
}

function checkKeysCriticalAttack(keys, pressed) {
  for (let key of keys) {
    if (!pressed.has(key)) {
      return false;
    }
  }
  return true;
}

function setHpInBar(fighter) {
  let percent = (fighter.health * 100) / fighter.fighter.health;
  if (percent < 0) percent = 0;
  fighter.hpContainer.style.width = `${percent}%`;
}
