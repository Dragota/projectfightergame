import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  // My
  let fragment = document.createDocumentFragment();
  if (fighter) {
    console.log(fighter);
    for (let characteristic in fighter) {
      if (characteristic === 'source') {
        const fighterImgElement = createElement({
          tagName: 'img',
          className: `fighter-preview--img`,
          attributes: {
            src: fighter[characteristic],
            title: fighter['name'],
            alt: fighter['name'],
          },
        });
        fighterElement.appendChild(fighterImgElement);
      } else if (characteristic !== 'source' && characteristic !== '_id') {
        const characteristicElement = createElement({
          tagName: 'p',
          className: `fighter-preview--${characteristic}`,
        });
        characteristicElement.innerText = `${characteristic[0].toUpperCase() + characteristic.slice(1)}: ${
          fighter[characteristic]
        }`;
        fragment.appendChild(characteristicElement);
      }
    }
    const containerForCharacteristic = createElement({
      tagName: 'div',
      className: `fighter-preview--container-characteristic`,
    });
    containerForCharacteristic.appendChild(fragment);
    fighterElement.appendChild(containerForCharacteristic);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
